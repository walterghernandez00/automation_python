while True:
    word = input("Enter a word with letter 'o': ")
    if 'o' in word or 'O' in word:
        break
    print("Try again, the word doesn't contain letter 'o'.")
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = [item for item in lst1 if isinstance(item, str)]
text = "Enter your text here."
word_list = text.split()
count = sum(1 for word in word_list if word.endswith('o') or word.endswith('O'))