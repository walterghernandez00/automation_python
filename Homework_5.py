#Завдання 1
visited_cities = input("Enter the cities you have visited in the past 10 years, separated by a space: ").lower().split()
planned_cities = input("Enter the cities you want to visit in the next 10 years, separated by a space: ").lower().split()

visited_cities_set = set(visited_cities)
planned_cities_set = set(planned_cities)

common_cities = visited_cities_set.intersection(planned_cities_set)

if common_cities:
    message = "It seems that you really enjoyed visiting " + ", ".join(list(common_cities)) + "."
    print(message)
else:
    print("You are open to new experiences.")

#Завдання 2
student = {
    'Іван Петров': {
        'Пошта': 'Ivan@gmail.com',
        'Вік': 14,
        'Номер телефону': '+380987771221',
        'Середній бал': 95.8
    },
    'Женя Курич': {
        'Пошта': 'Geka@gmail.com',
        'Вік': 16,
        'Номер телефону': None,
        'Середній бал': 64.5
    },
    'Маша Кера': {
        'Пошта': 'Masha@gmail.com',
        'Вік': 18,
        'Номер телефону': '+380986671221',
        'Середній бал': 80
    },
}
def add_student(student_dict, name, email, age, phone, grade):
    if age >= 18 and age <= 40:
        if grade >= 0 and grade <= 100:
            student_dict[name] = {
                'Пошта': email,
                'Вік': age,
                'Номер телефону': phone,
                'Середній бал': grade
            }
            print(f"Student {name} was added to the list.")
        else:
            print(f"Invalid grade: {grade}. The grade should be between 0 and 100.")
    else:
        print(f"Invalid age: {age}. The age should be between 18 and 40.")


def print_student_list(student_dict, grade_threshold):
    print("List of students with grade higher than", grade_threshold, ":")
    for name, info in student_dict.items():
        if info["Середній бал"] > grade_threshold:
            print(name, info["Середній бал"])


def average_grade(student_dict):
    total_grade = 0
    count = 0
    for info in student_dict.values():
        total_grade += info["Середній бал"]
        count += 1
    return total_grade / count


def handle_missing_phone_number(student_dict, phone_number):
    for name, info in student_dict.items():
        if info["Номер телефону"] == None:
            info["Номер телефону"] = phone_number
            print(f"Студент {name} немає номеру телефона, assigned {phone_number}.")


# Adding a student
add_student(student, "John Doe", "Johndoe@gmail.com", 25, "+1234567890", 92.0)

# Printing the list of students with grade higher than 90
print_student_list(student, 90)

# Calculating the average grade
average = average_grade(student)
print("The average grade in the group is", average)

# Handling missing phone numbers
handle_missing_phone_number(student, "No phone number available")

# Printing the updated list of students
print("\nUpdated student list:")
for name, info in student.items():
    print(name, info)

